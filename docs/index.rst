.. debuggie documentation master file, created by
   sphinx-quickstart on Tue Jan 14 09:40:53 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to debuggie's documentation!
====================================

Contents:

.. automodule:: debuggie
    :members:

.. automodule:: debuggie.api
    :members:

.. automodule:: debuggie.urls
    :members:

.. automodule:: debuggie.views
    :members:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

