# -*- coding: utf-8 -*-

u"""
debuggie.views
----------------
"""

from django.shortcuts import HttpResponseRedirect, render_to_response
from django.http import HttpResponseForbidden
from django.template.loader import render_to_string
from django.core.urlresolvers import reverse

import api


def debug_status(request):
    """
    Вывод текущего состояния логгера
    """
    return render_to_response(
        'debug_status.html',
        {
            'status': api.get_status(request),
            'download_url': api.get_download_url(request),
        }
    )


def debug_toggle(request):
    """
    Переключение состояния логгера
    """
    api.toggle_status(request)
    return HttpResponseRedirect(reverse(debug_status))


def debug_clear(request):
    """
    Очистка списка событий
    """
    api.clear_records(request)
    return HttpResponseRedirect(reverse(debug_status))
