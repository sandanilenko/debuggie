#coding: utf-8

from django.contrib.auth.models import User, AnonymousUser
from django.test import TestCase
from django.test.client import RequestFactory
from django.conf import settings
from django.utils.importlib import import_module

from views import debug_status, debug_toggle


class DebuggieTest(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username="test",
                                             email="test@test.local",
                                             password="test")
        self.factory = RequestFactory()

        settings.SESSION_ENGINE = 'django.contrib.sessions.backends.file'
        engine = import_module(settings.SESSION_ENGINE)
        store = engine.SessionStore()
        store.save()
        self.session = store
        self.client.cookies[settings.SESSION_COOKIE_NAME] = store.session_key

    def test_debug_status(self):
        request = self.factory.get('/debug/')
        request.session = self.session

        request.user = self.user
        response = debug_status(request)
        self.assertEqual(response.status_code, 200)

        request.user = AnonymousUser()
        response = debug_status(request)
        self.assertEqual(response.status_code, 403)

    def test_toggle_status(self):
        request = self.factory.get('/debug/toggle')
        request.session = self.session
        request.user = self.user

        request.session.pop('debug_enabled', False)

        response = debug_toggle(request)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response._headers['location'], ('Location', '/debug/'))
        self.assertIn('debug_enabled', request.session)
        self.assertEqual(request.session['debug_enabled'], True)

        response = debug_toggle(request)
        self.assertEqual(request.session['debug_enabled'], False)
