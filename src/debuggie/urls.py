# -*- coding: utf-8 -*-
u"""
debuggie.urls
----------------
"""
from django import VERSION as DJANGO_VER

# В Django >= 1.6 немного изменена структура модулей
if DJANGO_VER[:2] < (1,6):
	from django.conf.urls.defaults import patterns, url
else:
	from django.conf.urls import patterns, url

urlpatterns = patterns(
    '',
    url('^$', 'debuggie.views.debug_status'),
    url('^toggle$', 'debuggie.views.debug_toggle'),
    url('^clear$', 'debuggie.views.debug_clear'),
)
